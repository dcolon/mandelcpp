#include "mandel.h"

// Private

/// \brief Initialises viewport relative to window size.
void Mandel::initialiseViewport() {
	long double initialBound      = 1;
	long double initialBoundJulia = 1.5;
	uint16_t    width             = window->windowRect.right;
	uint16_t    height            = window->windowRect.top;
	// window->windowRect.right
	// Get <-2,2> in the smallest part (height or width)
	// Scale the other accordingly.
	if (width > height) {
		long double rel = width / height;
		viewport = {
			rel * -initialBound, // left
			rel * initialBound,  // right
			-initialBound,       // bottom
			initialBound         // top
		};
		juliaViewport = {
			rel * -initialBoundJulia, // left
			rel * initialBoundJulia,  // right
			-initialBoundJulia,       // bottom
			initialBoundJulia         // top
		};
	} else {
		long double rel = height / width;
		viewport = {
			-initialBound,       // left
			initialBound,        // right
			rel * -initialBound, // bottom
			rel * initialBound   // top
		};
		juliaViewport = {
			-initialBoundJulia,       // left
			initialBoundJulia,        // right
			rel * -initialBoundJulia, // bottom
			rel * initialBoundJulia   // top
		};
	}
	// Set position.
	position = {0, 0};
}

/// \brief Tries to init openCl.
void Mandel::initialiseOpenCl(const std::string& path) {
	opencl = Opencl();
	if(opencl.available) {
		opencl.loadProgramFromFile(path + "/assets/mandel.cl", "mandel");
		useOpenCl = opencl.initialised;
		if(useOpenCl) {
			std::cout << "Opencl: Succesfully initialised." << std::endl;
		} else {
			std::cout << "Opencl: Failed initialisation." << std::endl;
		}
	}
}

/// \brief Creates output dir if it doesn't exist.
void Mandel::initialiseSaveDir() {
	if (!std::filesystem::exists(saveDir)) {
		std::filesystem::create_directory(saveDir);
	}
}

/// \brief Initialises canvas texture.
void Mandel::initialiseTextures() {
	// Mandel window.
	window->generateRectTexture(&canvasTexture, window->windowRect.right, window->windowRect.top);
	window->generateCrossTexture(&mouseTexture, mouseSize, 2, mouseColor);
	// Julia window.
	juliaWindow->generateRectTexture(&juliaCanvasTexture, juliaWindow->windowRect.right, juliaWindow->windowRect.top);
}

/// \brief Calls SDL_DestoryTexture on all textures created at init
void Mandel::destroyTextures() {
	SDL_DestroyTexture(canvasTexture);
	SDL_DestroyTexture(juliaCanvasTexture);
	// Status texture might not be defined
	if(statusTexture) {
		SDL_DestroyTexture(statusTexture);
	}
}

/// \param AText New text for the status texture
/// \brief If the status text has changed it will be rendered to the statusTexture.
void Mandel::generateStatusTexture(const std::string& text) {
	if(text == statusText) {
		// The texture didn't change
		return;
	}
	statusText = text;
	window->generateTextTexture(&statusTexture, text, statusTextColor);
}

/// \param x On screen x coordinate.
/// \param y On screen y coordinate.
/// \brief Set simulation psoition from screen coordinates.
void Mandel::setPositionFromScreenCoordinates(int& x, int& y) {
	if (windowWidth == 0 || windowHeight == 0) {
		return;
	}
	// First calculate the position as a fraction (i.e. 1/4th down)/
	const long double xFraction = (long double)x / (long double)windowWidth;
	const long double yFraction = (long double)y / (long double)windowHeight;
	// Then calculate width of the viewport.
	const long double viewWidth  = viewport.right - viewport.left;
	const long double viewHeight = viewport.top - viewport.bottom;
	// Then calculate the position in the scale fo the viewport.
	// Shift by viewport boundaries.
	position = {
		viewport.left + (xFraction * viewWidth),
		viewport.bottom + (yFraction * viewHeight),
	};
}

// Public

/// \param APath Base path of the application, used for loading assets
///
/// \brief Instantiates the application.
///
/// - Initialises static properties for objects
/// - Creates an SDLWindow to render everything in.
Mandel::Mandel(const std::string& path) {
	// Use the path to find our font
	const std::string fontpath = path + "/assets/Inconsolata-Regular.ttf";
	saveDir                    = path + "/" + saveDir;
	// And launch our windows
	window      = new SDLWindow(fontpath, statusTextSize, windowWidth, windowHeight, 100, 100, "Mandel");
	juliaWindow = new SDLWindow(fontpath, statusTextSize, windowWidthJulia, windowHeightJulia, 1120, 100, "Julia");
	// textures
	initialiseTextures();
	// Init opencl;
	initialiseOpenCl(path);
	// Calculate position of the viewport.
	initialiseViewport();
	// Create save dir if it doesn't exist.
	initialiseSaveDir();
	renderMandel();
}

/// \brief Destroys everything neatly.
///
/// Destroys:
/// - Textures
Mandel::~Mandel() {
	destroyTextures();
}

/// \brief Main application loop
///
/// Runs while gracefulShutdown has not been set
/// Every iteration it will:
/// - Update the draw status
/// - Rerender or draw if required
/// - Get events from the window and handle them
///
/// \todo Move event handling to separate function
void Mandel::run() {
	// Wait for close
	bool ctrlPressed = false;
	bool qPressed    = false;
	bool cPressed    = false;
	bool rPressed    = false;
	bool mouseDown   = false;
	// The main loop
	while(!gracefulShutdown) {
		// Present to screen if we have to
		// Use vsync to limit the framerate by drawing every iteration
		if(needDraw) {
			renderMandel();
			renderMousePosition();
			if(drawStatus) {
				renderStatus();
			}
			window->draw();
			needDraw = false;
		}
		if(needDrawJulia) {
			renderJulia();
			juliaWindow->draw();
			if(recording) {
				const std::string path = saveDir + std::to_string(saveFrame++) + ".bmp";
				juliaWindow->saveScreenshot(path);
			}
			needDrawJulia = false;
		}
		// getEvent will wait until input is recieved. We won't need to do anything
		// until then.
		while(!gracefulShutdown &&  window->getEvent(event, false)) {
			//User requests quit
			if(event.type == SDL_QUIT) {
				gracefulShutdown = true;
			} else if(event.type == SDL_KEYDOWN) {
				long double scaleX = 1;
				long double scaleY = 1;
				// used in moving (so we move relative to scale).
				long double xSize = abs(viewport.right - viewport.left);
				long double ySize = abs(viewport.top - viewport.bottom);
				switch(event.key.keysym.sym) {
				case SDLK_LCTRL:
					ctrlPressed = true;
					break;
				case SDLK_q:
					qPressed = true;
					break;
				case SDLK_w:
					// Track state.
					needDraw = true;
					// Move viewport.
					viewport.top    -= positionStep * ySize;
					viewport.bottom -= positionStep * ySize;
					break;
				case SDLK_a:
					// Track state.
					needDraw = true;
					// Move viewport.
					viewport.right -= positionStep * xSize;
					viewport.left  -= positionStep * xSize;
					break;
				case SDLK_s:
					// Track state.
					needDraw = true;
					// Move viewport.
					viewport.top    += positionStep * ySize;
					viewport.bottom += positionStep * ySize;
					break;
				case SDLK_d:
					// Track state.
					needDraw = true;
					// Move viewport.
					viewport.right += positionStep * xSize;
					viewport.left  += positionStep * xSize;
					break;
				case SDLK_UP:
					// Track state.
					needDraw = true;
					// Zoom in.
					scaleX           = scaleStep * xSize;
					scaleY           = scaleStep * ySize;
					viewport.right  -= scaleX;
					viewport.left   += scaleX;
					viewport.top    -= scaleY;
					viewport.bottom += scaleY;
					break;
				case SDLK_DOWN:
					// Track state.
					needDraw = true;
					// Zoom out.
					scaleX           = -scaleStep * xSize;
					scaleY           = -scaleStep * ySize;
					viewport.right  -= scaleX;
					viewport.left   += scaleX;
					viewport.top    -= scaleY;
					viewport.bottom += scaleY;
					break;
				case SDLK_c:
					if(!cPressed) {
						// Track state.
						cPressed = true;
						if (opencl.available && opencl.initialised) {
							useOpenCl = !useOpenCl;
							needDraw  = true;
						}
					}
					break;
				case SDLK_r:
					if(!rPressed) {
						// Track state.
						rPressed      = true;
						recording     = !recording;
						needDrawJulia = true;
					}
					break;
				}
			} else if(event.type == SDL_KEYUP) {
				switch(event.key.keysym.sym) {
				case SDLK_LCTRL:
					ctrlPressed = false;
					break;
				case SDLK_q:
					qPressed = false;
					break;
				case SDLK_c:
					cPressed = false;
					break;
				case SDLK_r:
					rPressed = false;
					break;
				}
			} else if(event.type == SDL_WINDOWEVENT) {
				switch(event.window.event) {
				case SDL_WINDOWEVENT_RESIZED:
					printf("Resized: Width: %u height: %u\n", event.window.data1, event.window.data2);
					break;
				case SDL_WINDOWEVENT_SIZE_CHANGED:
					// Keep track of sizes
					window->windowRect.right = event.window.data1;
					window->windowRect.top   = event.window.data2;
					printf("Sizechanged: Width: %u height: %u\n", window->windowRect.right, window->windowRect.top);
					break;
				}
			} else if(event.type == SDL_MOUSEBUTTONDOWN) {
				// Update state.
				mouseDown     = true;
				needDraw      = true;
				needDrawJulia = true;
				//Get mouse position
				int x, y;
				SDL_GetMouseState(&x, &y);
				mousePosition = {x, y};
				setPositionFromScreenCoordinates(x, y);
			} else if(event.type == SDL_MOUSEMOTION && mouseDown) {
				// Update state.
				needDraw      = true;
				needDrawJulia = true;
				//Get mouse position
				int x, y;
				SDL_GetMouseState(&x, &y);
				mousePosition = {x, y};
				setPositionFromScreenCoordinates(x, y);
			} else if(event.type == SDL_MOUSEBUTTONUP) {
				// Update state.
				mouseDown     = false;
			}
			if(ctrlPressed) {
				if(qPressed) {
					gracefulShutdown = true;
				}
			}
		}
	}
}

void Mandel::renderMandel() {
	// Get the size of the window. (pixels)
	const uint16_t widthP  = window->windowRect.right;
	const uint16_t heightP = window->windowRect.top;
	// Create a buffer large enough to hold the same
	// number of pixels as the GPU-side texture.
	// 4 is the size in bytes of a single RGBA pixel.
	uint64_t numPixels     = widthP * heightP;
	uint64_t numPixelChars = numPixels * 4;
	char*    pixels        = (char*)malloc(numPixelChars);
	int      pitch         = widthP * 4;
	uint64_t pixelIndex    = 0;
	// Calculate the width of the viewport
	const long double width  = viewport.right - viewport.left;
	const long double height = viewport.top - viewport.bottom;
	// Calculate the scale factors in x and y so that:
	// pixel * scale = coord
	const long double scaleX = width / widthP;
	const long double scaleY = height / heightP;
	// Now render in opencl or cpu
	if(useOpenCl) {
		// Opencl
		// Resize vector buffers.
		cInReal.resize(numPixels);
		cInImag.resize(numPixels);
		zInReal.resize(numPixels);
		zInImag.resize(numPixels);
		// Create buffers.
		cl::Buffer cRealInBuffer(opencl.context, CL_MEM_READ_ONLY, sizeof(cl_double)*numPixels);
		cl::Buffer cImagInBuffer(opencl.context, CL_MEM_READ_ONLY, sizeof(cl_double)*numPixels);
		cl::Buffer zRealInBuffer(opencl.context, CL_MEM_READ_ONLY, sizeof(cl_double)*numPixels);
		cl::Buffer zImagInBuffer(opencl.context, CL_MEM_READ_ONLY, sizeof(cl_double)*numPixels);
		cl::Buffer outBuffer(opencl.context, CL_MEM_WRITE_ONLY, sizeof(char)*numPixelChars);
		// Fill the input buffers.
		for (uint16_t yp = 0; yp < heightP; yp++) {
			for (uint16_t xp = 0; xp < widthP; xp++) {
				uint32_t coord = yp * widthP + xp;
				cInReal.at(coord) = viewport.left + (xp * scaleX);
				cInImag.at(coord) = viewport.bottom + (yp * scaleY);
				zInReal.at(coord) = 0;
				zInImag.at(coord) = 0;
			}
		}
		// Write them to the device.
		opencl.queue.enqueueWriteBuffer(cRealInBuffer, CL_TRUE, 0, sizeof(cl_double)*numPixels, (void*)&cInReal.front());
		opencl.queue.enqueueWriteBuffer(cImagInBuffer, CL_TRUE, 0, sizeof(cl_double)*numPixels, (void*)&cInImag.front());
		opencl.queue.enqueueWriteBuffer(zRealInBuffer, CL_TRUE, 0, sizeof(cl_double)*numPixels, (void*)&zInReal.front());
		opencl.queue.enqueueWriteBuffer(zImagInBuffer, CL_TRUE, 0, sizeof(cl_double)*numPixels, (void*)&zInImag.front());
		// Set parameters to the kernel.
		cl::Kernel* kernel = opencl.kernels.at("mandel");
		kernel->setArg(0, cRealInBuffer);
		kernel->setArg(1, cImagInBuffer);
		kernel->setArg(2, zRealInBuffer);
		kernel->setArg(3, zImagInBuffer);
		kernel->setArg(4, outBuffer);
		// Enqueue the kernel.
		opencl.queue.enqueueNDRangeKernel(*kernel, cl::NullRange, cl::NDRange(numPixels), cl::NullRange);
		// Then read the outptu intot he pixel buffer making sure to block.
		opencl.queue.enqueueReadBuffer(outBuffer, CL_TRUE, 0,sizeof(char)*numPixelChars, pixels);
	} else {
		// CPU
		// Iterate over pixels in y.
		for (uint16_t yp = 0; yp < heightP; yp++) {
			// Convert out pixels to coordinates from the viewport.
			// Shift by halfheight so our origin is in the center.
			long double y = viewport.bottom + (yp * scaleY);
			// And in x.
			for (uint16_t xp = 0; xp < widthP; xp++) {
				// Convert out pixels to coordinates form the bounds.
				// Shift by halfwidth so our origin is in the center.
				long double x = viewport.left + (xp * scaleX);
				// Iterate over the mandelbrot set.
				uint16_t iterated = Fractal::mandelBrot(x, y, iterations);
				pixelIndex = (yp * widthP + xp) * 4;
				// Then draw the pixel in greyscale.
				((char*)pixels)[pixelIndex + 3] =   256 - (iterated * resolution);
				((char*)pixels)[pixelIndex + 1] =   256 - (iterated * resolution);
				((char*)pixels)[pixelIndex + 2] =   256 - (iterated * resolution);
				((char*)pixels)[pixelIndex + 0] =   255;
			}
		}
	}
	if (SDL_UpdateTexture(canvasTexture, NULL, pixels, pitch) < 0) {
		std::cout << "Failed to update " << SDL_GetError() << std::endl;
	}
	//SDL_UnlockTexture(canvasTexture);
	SDL_RenderCopy(window->renderer, canvasTexture, NULL, NULL);
	//SDL_RenderPresent(window->renderer);
}

void Mandel::renderJulia() {
	// Get the size of the window. (pixels)
	const uint16_t widthP  = juliaWindow->windowRect.right;
	const uint16_t heightP = juliaWindow->windowRect.top;
	// Create a buffer large enough to hold the same
	// number of pixels as the GPU-side texture.
	// 4 is the size in bytes of a single RGBA pixel.
	uint64_t numPixels     = widthP * heightP;
	uint64_t numPixelChars = numPixels * 4;
	char*    pixels        = (char*)malloc(numPixelChars);
	int      pitch         = widthP * 4;
	uint64_t pixelIndex    = 0;
	// Calculate the width of the viewport
	const long double width  = juliaViewport.right - juliaViewport.left;
	const long double height = juliaViewport.top - juliaViewport.bottom;
	// Calculate the scale factors in x and y so that:
	// pixel * scale = coord
	const long double scaleX = width / widthP;
	const long double scaleY = height / heightP;
	// Now render in opencl or cpu
	if(useOpenCl) {
		// Opencl
		// Resize vector buffers.
		cInReal.resize(numPixels);
		cInImag.resize(numPixels);
		zInReal.resize(numPixels);
		zInImag.resize(numPixels);
		// Create buffers.
		cl::Buffer cRealInBuffer(opencl.context, CL_MEM_READ_ONLY, sizeof(cl_double)*numPixels);
		cl::Buffer cImagInBuffer(opencl.context, CL_MEM_READ_ONLY, sizeof(cl_double)*numPixels);
		cl::Buffer zRealInBuffer(opencl.context, CL_MEM_READ_ONLY, sizeof(cl_double)*numPixels);
		cl::Buffer zImagInBuffer(opencl.context, CL_MEM_READ_ONLY, sizeof(cl_double)*numPixels);
		cl::Buffer outBuffer(opencl.context, CL_MEM_WRITE_ONLY, sizeof(char)*numPixelChars);
		// Fill the input buffers.
		for (uint16_t yp = 0; yp < heightP; yp++) {
			for (uint16_t xp = 0; xp < widthP; xp++) {
				uint32_t coord = yp * widthP + xp;
				cInReal.at(coord) = position.x;
				cInImag.at(coord) = position.y;
				zInImag.at(coord) = juliaViewport.bottom + (yp * scaleY);
				zInReal.at(coord) = juliaViewport.left + (xp * scaleX);
			}
		}
		// Write them to the device.
		opencl.queue.enqueueWriteBuffer(cRealInBuffer, CL_TRUE, 0, sizeof(cl_double)*numPixels, (void*)&cInReal.front());
		opencl.queue.enqueueWriteBuffer(cImagInBuffer, CL_TRUE, 0, sizeof(cl_double)*numPixels, (void*)&cInImag.front());
		opencl.queue.enqueueWriteBuffer(zRealInBuffer, CL_TRUE, 0, sizeof(cl_double)*numPixels, (void*)&zInReal.front());
		opencl.queue.enqueueWriteBuffer(zImagInBuffer, CL_TRUE, 0, sizeof(cl_double)*numPixels, (void*)&zInImag.front());
		// Set parameters to the kernel.
		cl::Kernel* kernel = opencl.kernels.at("mandel");
		kernel->setArg(0, cRealInBuffer);
		kernel->setArg(1, cImagInBuffer);
		kernel->setArg(2, zRealInBuffer);
		kernel->setArg(3, zImagInBuffer);
		kernel->setArg(4, outBuffer);
		// Enqueue the kernel.
		opencl.queue.enqueueNDRangeKernel(*kernel, cl::NullRange, cl::NDRange(numPixels), cl::NullRange);
		// Then read the outptu intot he pixel buffer making sure to block.
		opencl.queue.enqueueReadBuffer(outBuffer, CL_TRUE, 0,sizeof(char)*numPixelChars, pixels);
	} else {
		// CPU
		// Iterate over pixels in y.
		for (uint16_t yp = 0; yp < heightP; yp++) {
			// Convert out pixels to coordinates from the viewport.
			// Shift by halfheight so our origin is in the center.
			long double y = juliaViewport.bottom + (yp * scaleY);
			// And in x.
			for (uint16_t xp = 0; xp < widthP; xp++) {
				// Convert out pixels to coordinates form the bounds.
				// Shift by halfwidth so our origin is in the center.
				long double x = juliaViewport.left + (xp * scaleX);
				// Iterate over the mandelbrot set.
				uint16_t iterated = Fractal::julia(position.x, position.y, x, y, iterations);
				pixelIndex = (yp * widthP + xp) * 4;
				// Then draw the pixel in greyscale.
				((char*)pixels)[pixelIndex + 3] =   256 - (iterated * resolution);
				((char*)pixels)[pixelIndex + 1] =   256 - (iterated * resolution);
				((char*)pixels)[pixelIndex + 2] =   256 - (iterated * resolution);
				((char*)pixels)[pixelIndex + 0] =   255;
			}
		}
	}
	if (SDL_UpdateTexture(juliaCanvasTexture, NULL, pixels, pitch) < 0) {
		std::cout << "Failed to update " << SDL_GetError() << std::endl;
	}
	//SDL_UnlockTexture(canvasTexture);
	SDL_RenderCopy(juliaWindow->renderer, juliaCanvasTexture, NULL, NULL);
}

/// @brief Renders the julia canvas texture onto the julia window.
void Mandel::renderJuliaCanvas() {
	// Create a source and dest rect (same size and position).
	const SDL_Rect rect = {juliaWindow->windowRect.left, juliaWindow->windowRect.right, juliaWindow->windowRect.top, juliaWindow->windowRect.bottom};
	// Blit onto window.
	juliaWindow->renderTextureCopy(juliaCanvasTexture, rect, rect);
}

/// @brief Renders the canvas texture onto the window.
void Mandel::renderCanvas() {
	// Create a source and dest rect (same size and position).
	const SDL_Rect rect = {window->windowRect.left, window->windowRect.right, window->windowRect.top, window->windowRect.bottom};
	// Blit onto window.
	window->renderTextureCopy(canvasTexture, rect, rect);
}

/// \brief Creates a status texture from the current population. Draw to screen if necessary.
///
/// Draws it to the screen if the text has changed or if a redraw is needed
///
/// \note Sets needDraw to true
void Mandel::renderStatus() {
	// Generate status text
	// Get some coords.
	std::ostringstream oss;
	oss.precision(5);
    oss << std::fixed << position.x << "+" << position.y << "i";
	//long double x          = viewport.left + ((viewport.right - viewport.left) / 2);
	//long double y          = viewport.bottom + ((viewport.top - viewport.bottom) / 2);
	//std::string statusText = "X: " + std::to_string(x) + " Y: " + std::to_string(y);
	std::string useOpenClString = useOpenCl ? "yes " : "no ";
	std::string recordingString = recording ? "yes " : "no ";
	std::string statusText      = "openCl: " + useOpenClString + "rec: " + recordingString + oss.str();
	// Render text onto a texture
	generateStatusTexture(statusText);
	// Query texture size
	int textWidth, textHeight;
	SDL_QueryTexture(statusTexture, NULL, NULL, &textWidth, &textHeight);
	// Get the coordinates of the top of the colored background bar
	int barTop = window->windowRect.top - statusPadding - textHeight - statusPadding;
	// Make rects (These are SDL_Rects!)
	const SDL_Rect srcRect = {0, 0, textWidth, textHeight};
	const SDL_Rect dstRect = {statusPadding, barTop + statusPadding, textWidth, textHeight};
	// Draw background bar
	window->renderFilledRect(Rect{window->windowRect.left, window->windowRect.right, window->windowRect.top, barTop}, statusBkgColor);
	// Copy our texture to the screen
	window->renderTextureCopy(statusTexture, srcRect, dstRect);
	needDraw = true;
}

/// \brief Renders the mouse position onto the canvas.
void Mandel::renderMousePosition() {
	// Make rects to copy the mouse texture.
	uint8_t halfSize       = mouseSize / 2;
	const SDL_Rect srcRect = {0, 0, mouseSize, mouseSize};
	const SDL_Rect dstRect = {mousePosition.x - halfSize, mousePosition.y - halfSize, mouseSize, mouseSize};
	// Copy our texture to the screen
	window->renderTextureCopy(mouseTexture, srcRect, dstRect);
}