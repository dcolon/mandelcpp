__kernel void mandel(global const double* cRealBuffer, global const double* cImagBuffer, global const double* zRealBuffer, global const double* zImagBuffer, global char* resultBuffer) {
	// Init variables.
	double zRealSq = 0;
	double zImagSq = 0;
	// We first calculate the magnitude |a+b1|
	double cReal = cRealBuffer[get_global_id(0)];
	double cImag = cImagBuffer[get_global_id(0)];
	double zReal = zRealBuffer[get_global_id(0)];
	double zImag = zImagBuffer[get_global_id(0)];
	uint i       = 0;
	// Then we iterate,
	// We will return if:
	//   i is larger than the maximum number of iterations.
	//   the magnitude of Z >= 2 (We're cutting out the sqrt for speed.)
	while (i++ < 255) {
	  // We'll only calculate the squares once.
	  zRealSq = zReal * zReal;
	  zImagSq = zImag * zImag;
	  // Did we escape the mandelbrot set?
	  if ((zRealSq + zImagSq) > 4) {
		break;
	  }
	  // otherwise calculate the next iteration.
	  zImag = 2 * zReal * zImag + cImag;
	  zReal = zRealSq - zImagSq + cReal;
	}
	// We have four bytes per index. RGBA.
	int resultIndex = get_global_id(0) * 4;
	resultBuffer[resultIndex + 1] = 256 - i;
	resultBuffer[resultIndex + 2] = 256 - i;
	resultBuffer[resultIndex + 3] = 256 - i;
	resultBuffer[resultIndex + 0] = 255;
}
