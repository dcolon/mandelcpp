#include <unistd.h>
#include <iostream>
#include "mandel.h"

/// \brief main
/// \return 0 if exited without errors
int main(int argc, char* args[]) {
	std::cout << "Starting Mandelcpp" << std::endl;
	// Get the dir the application resides in
	std::string path   = args[0];
	size_t      dirEnd = path.find_last_of('/');
	path               = path.substr(0, dirEnd);
	// Initialise and start the simulation
	Mandel mandel = Mandel(path);
	mandel.run();
	return 0;
}
