#ifndef MANDEL_H
#define MANDEL_H

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <random>
#include <list>
#include <ctime>
#include <filesystem>
#include "lib/sdl/geometry.h"
#include "lib/sdl/sdlwindow.h"
#include "lib/fractal/fractal.h"
#include "lib/opencl/opencl.h"
#include "utils/dccolor.h"

/// \brief Handles the simulation logic and contains the model
class Mandel {
	private:
		/// @name Window settings:
		/// @{
		/// \brief Constant settings for the window.
		const uint16_t    resolution        = 1;    ///< Resolution to render at. Power of 2! Lower is higher detail but slower.
		const uint16_t    iterations        = 255;  ///< Max number of iterations over the mandelbrot function per pixel.
		const long double positionStep      = 0.1;  ///< Relative step when moving viewport.
		const long double scaleStep         = 0.1;  ///< Relative step when zooming.
		const uint16_t    windowWidth       = 500; ///< Width of screen upon start.
		const uint16_t    windowHeight      = 500; ///< Height of screen upon start.
		const uint16_t    windowWidthJulia  = 500;  ///< Width of julia screen upon start.
		const uint16_t    windowHeightJulia = 500;  ///< Height of julia screen upon start.
		const uint8_t     mouseSize         = 10;   ///< Size of the mouse position.
		/// @}

		/// @name Misc settings:
		/// @{
		/// \brief Misc settings
		std::string saveDir = "output/";
		/// @}

		/// @name Color settings:
		/// @{
		/// \brief Constant settings for drawing
		const DCColor::RgbaColor backgroundColor = {255, 255, 255, 255}; ///< Background color to render
		const DCColor::RgbaColor pixelColor      = {0, 0, 255, 255};     ///< Background color to render
		// Status
		const uint8_t            statusTextSize  = 14;                   ///< Size of the status bar text
		const uint8_t            statusPadding   = 5;                    ///< Padding of the status bar
		const DCColor::RgbaColor statusTextColor = {255, 255, 255, 255}; ///< Color of the status text
		const DCColor::RgbaColor statusBkgColor  = {0, 0, 0, 100};       ///< Color of the status background
		// Mouse
		const DCColor::RgbaColor mouseColor      = {89, 98, 255, 255};   ///< Color of the mouse cursor
		/// @}

		/// @name Window
		/// @{
		/// \brief The window the simulation is displayed in and related items
		///
		/// If a need for a redraw is detected during the main loop needRedraw is set.
		/// At the start of the next iteration the window will be cleared.
		/// needRedraw will be set to false and doingRedraw will be set. needDraw will be set as well.
		/// This will cause every object being iterated over to be rendered to the screen.
		/// This means there can be a one-frame lag in drawing objects.
		SDLWindow* window;      ///< The SDLWindow displaying the mandelbrot set
		SDLWindow* juliaWindow; ///< The SDLWindows displaying the corresponding Julia set.
		SDL_Event  event;       ///< Events read from the window like button events

		/// Draw status
		bool needDraw      = true;     ///< Set to true when something has been rendered and needs to be presented on the screen
		bool needDrawJulia = true;

		// Textures
		SDL_Texture* canvasTexture      = nullptr; ///< Main render target for mandelbrot set.
		SDL_Texture* juliaCanvasTexture = nullptr; ///< Main render target for mandelbrot set.
		SDL_Texture* statusTexture      = nullptr; ///< Texture used when rendering the status.
		SDL_Texture* mouseTexture       = nullptr; ///< Mous eposition texture.
		/// @}

		/// @name OpenCl
		/// @{
		/// \brief The OpenCl interface.
		Opencl opencl;            ///< The opencl wrapper class.
		bool   useOpenCl = false; ///< Whether the calculations should use opencl or not.
		// Input and output buffers on controller.
		std::vector<double> cInReal;
		std::vector<double> cInImag;
		std::vector<double> zInReal;
		std::vector<double> zInImag;
		/// @}

		/// @name Toggles:
		/// @{
		/// \brief Bools that can be toggled through user interaction to control the application
		bool gracefulShutdown = false; ///< Whether a shutdown has been requested by the user
		bool drawStatus       = true;  ///< Whether the status bar should be shown
		/// @}

		/// @name State:
		/// @{
		/// \brief State of the screen and position.
		// Model
		LongDoubleRect viewport;          ///< Coordinates for rendered viewport.
		LongDoubleRect juliaViewport;     ///< Coordinates for rendered viewport of the julia set.
		LongDoubleVec  position;          ///< Position for Julia set.
		Vec            mousePosition;     ///< Position in screen coordinates.
		std::string    statusText = "";   ///< Text to render in the status bar
		uint64_t       saveFrame = 0;     ///< Frame for saving.
		bool		   recording = false; ///< Whether we're recording.
		/// @}

		// Init.
		void initialiseViewport();
		void initialiseOpenCl(const std::string& path);
		void initialiseSaveDir();

		// Graphics
		void initialiseTextures();
		void destroyTextures();
		void generateStatusTexture(const std::string& text);

		/// Helpers.
		void setPositionFromScreenCoordinates(int& x, int& y);
	public:

		// Initialisation
		Mandel(const std::string& path);
		~Mandel();
		// Simulation
		void run();
		// Rendering
		void renderMandel();
		void renderJulia();
		void renderCanvas();
		void renderJuliaCanvas();
		void renderStatus();
		void renderMousePosition();
};

#endif // MANDEL_H
