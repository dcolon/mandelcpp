#include "fractal.h"

/// @param cReal Real part of the starting number.
/// @param cImag Imaginary part of the starting number.
/// @param maxIterations Maximum number of iterations before returning.
/// @return uint16_t Number of iterations done.
/// @brief Iterates over the madelbrot function. Returns iterations.
///
/// Calls the julia funciton with z = 0
uint16_t Fractal::mandelBrot(const long double cReal, const long double cImag, const uint16_t maxIterations = 255) {
  return julia(cReal, cImag, 0, 0, maxIterations);
}

/// @param cReal Real part of the starting number.
/// @param cImag Imaginary part of the starting number.
/// @param zReal Real part of the starting power base Z.
/// @param zImag Imaginary part of the starting power base Z.
/// @param real Real part of the starting number.
/// @param imag Imaginary part of the starting number.
/// @param maxIterations Maximum number of iterations before returning.
/// @return uint16_t Number of iterations done.
/// @brief Iterates over the madelbrot function. Returns iterations.
///
/// Will iterate over the julia function Zn+1 = Zn^2 + C.
/// Will iterate until |Z| >= 2 or n >= maxIterations.
uint16_t Fractal::julia(const long double cReal, const long double cImag, long double zReal, long double zImag, const uint16_t maxIterations) {
  // Init variables.
  long double zRealSq = 0;
  long double zImagSq = 0;
  uint16_t i          = 0;
  // Then we iterate,
  // We will return if:
  //   i is larger than the maximum number of iterations.
  //   the magnitude of Z >= 2 (We're cutting out the sqrt for speed.)
  while (i++ < maxIterations) {
    // We'll only calculate the squares once.
    zRealSq = pow(zReal, 2);
    zImagSq = pow(zImag, 2);
    // Did we escape the mandelbrot set?
    if ((zRealSq + zImagSq) > 4) {
      break;
    }
    // otherwise calculate the next iteration.
    zImag = 2 * zReal * zImag + cImag;
    zReal = zRealSq - zImagSq + cReal;
  }
  return i;
}
