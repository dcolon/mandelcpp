#ifndef FRACTAL_H
#define FRACTAL_H

#include <cmath>
#include <cstdint>

/// \brief A class containing static functions for fractal iterations.
class Fractal final {
	public:
		static uint16_t mandelBrot(const long double cReal, const long double cImag, const uint16_t maxIterations);
		static uint16_t julia(const long double cReal, const long double cImag, long double zReal, long double zImag, const uint16_t maxIterations);
};

#endif //FRACTAL_H