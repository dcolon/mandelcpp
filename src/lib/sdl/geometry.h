#ifndef GEOMETRY_H
#define GEOMETRY_H

/// \brief A two-dimensional vector
struct Vec {
	int x;
	int y;
};

/// \brief A two-dimensional vector of long doubles.
struct LongDoubleVec {
	long double x;
	long double y;
};

/// \brief A rectangle. Defined by left, right, bottom, top
struct Rect {
	int left;
	int right;
	int bottom;
	int top;
};

/// \brief A rectangle of floating points. Defined by left, right, bottom, top
struct LongDoubleRect {
	long double left;
	long double right;
	long double bottom;
	long double top;
};

/// \brief A circle defined by a radius
struct Circ {
	int radius;
};

#endif // GEOMETRY_H
