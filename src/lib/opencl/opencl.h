#ifndef OPENCL_H
#define OPENCL_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <CL/cl.hpp>
#include <map>

class Opencl final {
	public:
		// Create/destroy.
		Opencl();
		~Opencl();
		// State.
		bool available   = false;
		bool initialised = false;
		// Functions
		void loadProgramFromFile(const std::string path, const std::string kernelName);
		// Components.
		cl::Device                         device;
		cl::Context                        context;
		cl::Program::Sources               sources;
		cl::Program                        program;
		cl::CommandQueue                   queue;
		std::map<std::string, cl::Kernel*> kernels;
};

#endif // OPENCL_H
