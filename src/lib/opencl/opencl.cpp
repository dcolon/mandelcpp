#include "opencl.h"

Opencl::Opencl() {
	std::cout << "Opencl: Initialising opencl" << std::endl << std::endl;

	//get all platforms (drivers)
	std::vector<cl::Platform> platforms;
	cl::Platform::get(&platforms);
	if(platforms.size()==0) {
		std::cout << "Opencl: No platforms found. Check OpenCL installation!" << std::endl;
		return;
	}
	// Print the platforms.
	std::cout << "Opencl: Available platforms:" << std::endl;
	for(std::vector<cl::Platform>::iterator it = platforms.begin(); it != platforms.end(); it++) {
		std::cout << it->getInfo<CL_PLATFORM_NAME>() << std::endl;
	}
	std::cout << std::endl;

	// Pick the default.
	cl::Platform defaultPlatform = platforms[0];
	std::cout << "Opencl: Using platform: " << defaultPlatform.getInfo<CL_PLATFORM_NAME>() << std::endl << std::endl;

	//get default device of the default platform
	std::vector<cl::Device> devices;
	defaultPlatform.getDevices(CL_DEVICE_TYPE_ALL, &devices);

	if(devices.size()==0) {
		std::cout << "Opencl: No devices found. Check OpenCL installation!" << std::endl;
		return;
	}

	// Print the devices.
	std::cout << "Opencl: Available devices:" << std::endl;
	for(std::vector<cl::Device>::iterator it = devices.begin(); it != devices.end(); it++) {
		std::cout << it->getInfo<CL_DEVICE_NAME>() << std::endl;
	}
	std::cout << std::endl;

	device=devices[0];
	std::cout << "Opencl: Using device: " << device.getInfo<CL_DEVICE_NAME>() << std::endl << std::endl;

	// Create context
	context = cl::Context({device});

	// And finally the command queue;
	queue = cl::CommandQueue(context, device);

	// Set initalised and available to true if we got here.
	available = true;
}

Opencl::~Opencl() {
	// Clean up the ekrnels we created.
	std::map<std::string, cl::Kernel*>::iterator iterator;
	for (iterator = kernels.begin(); iterator != kernels.end(); ++iterator) {
		delete iterator->second;
	}
}

void Opencl::loadProgramFromFile(const std::string path, const std::string kernelName) {
	// read the file into a buffer.
	std::ifstream file(path);
	if(file.fail()) {
		std::cout << "Opencl: failed opening file: " << path << std::endl;
		return;
	}
	std::stringstream buffer;
	buffer << file.rdbuf();
	std::string programString = buffer.str();
	// Read that into the sources.
	sources.push_back({programString.c_str(), programString.length()});
	// And attempt to compile the program.
	program = cl::Program(context, sources);
	if(program.build({device}) != CL_SUCCESS) {
		std::cout << "Opencl: Error building: " << program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device) << std::endl;
		return;
	}
	// Then extract the kernel.
	cl::Kernel* kernel = new cl::Kernel(program, kernelName.c_str());
	kernels.insert({kernelName, kernel});
	initialised = true;
}
