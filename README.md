# Mandel
## Introduction
A mandelbrot set application written in C++ using SDL2 for graphics.

## Dependencies
* libSDL2
* libSDL2_ttf
* Doxygen for docs

## Building
Simply run `make all`.
Run `make docs`.

## Running
Navigate to the build directory, make sure 'mandelcpp' is executable and run it.

## Output
To convert output BMP to other formats (in the saved dir):
`mogrify -channel RGB -negate -format png *.bmp && rm *.bmp`